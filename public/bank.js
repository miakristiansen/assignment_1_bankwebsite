//All html elements used in code - declared here
const computersElement = document.getElementById("computerSelect");
const specificationsElement = document.getElementById("specifications");
const computerPriceElement = document.getElementById("computerPrice");
const computerNameElement = document.getElementById("computerName")
const computerDescription = document.getElementById("computerDescription");
const imageElement = document.getElementById("image");
const payAmountElement = document.getElementById("payAmountTag");
const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstandingLoanSpan");
const loanHeadingElement = document.getElementById("loanHeading");
const payAmountBtnElement = document.getElementById("payAmountBtn")


let computers = []
let payAmount = 0;
let costOfComputer = 0;
let bankBalance = 0;
let loanAmount = 0;
let index = 0;
let computerBought = false;
let loanTaken = false;
const baseurl = "https://noroff-komputer-store-api.herokuapp.com/"


fetch(baseurl + "computers")//fetching data from api path
    .then(response => response.json())//parsing json response from fetch
    .then(data => computers = data)//adding data from api to array of computers
    .then(computers => addComputersToSelect(computers)); //passing computers array to addComputersToSelect

const addComputersToSelect = (computers) => {
    computers.forEach(x => addComputerToSelect(x));//Loop through array and sending a computer element to addComputerToSelect

    //setting default values to the site
    computerPriceElement.innerText = computers[0].price + " NOK"
    specificationsElement.innerText = computers[0].specs[0]
    computerNameElement.innerText = computers[0].title
    computerDescription.innerText = computers[0].description
    imageElement.src = baseurl+computers[0].image
    costOfComputer = parseInt(computers[0].price)
    //set the css value to display none
    hidePayButton();

}

//Sets the computer in select
const addComputerToSelect = (computer) => {
    const singleComputerElement = document.createElement('option'); //makes a option for each computer
    singleComputerElement.value = computer.id; //
    //appends a text node with the text = the title to computer to the option element
    singleComputerElement.appendChild(document.createTextNode(computer.title));
    //append the option element created to the select
    computersElement.appendChild(singleComputerElement);
}

//if the select change option
const handleComputerChange = event => {
    //from the computers array get the element targeted
    const selectedComputer = computers[event.target.selectedIndex];
    //Set new values
    computerPriceElement.innerText = selectedComputer.price + " NOK";
    specificationsElement.innerText = selectedComputer.specs;
    computerNameElement.innerText = selectedComputer.title;
    computerDescription.innerText = selectedComputer.description;
    imageElement.src = baseurl+selectedComputer.image;
    //parse the string to an int
    costOfComputer = parseInt(selectedComputer.price)
    //hold the index to track which one is selected - used later when user buys a computer
    index = event.target.selectedIndex
}

//on work button click the pay increases by a 100 everytime and change the pay display
function workBtnClick(){
    payAmount+=100;
    displayPay();
}
//on bank button click
function bankBtnClick(){

    //check if the pay amount is less the or equal to zero, prompt a message to user and returns from function
    if(payAmount <= 0){
        alert("You have nothing to put in the bank")
        return
    }
    //if loan greater then zero it should take 10% from the amount transferred to the bank and deduct
    // the bank balance with the 10%
    if(loanAmount > 0) {
        bankBalance += payAmount * 0.9
        loanAmount -= payAmount * 0.1
        displayOutstandingLoan();
    }else{
        //If no lian add the amount in full
        bankBalance = payAmount+bankBalance;
    }
        //Changes the html to display the current amount for pay and bank balance and sets the pay amount to 0
        displayBankBalance();
        payAmount = 0;
        displayPay();
}

//onclick event for buy now button
function buyNowClicked(){

    //checks if user have enough money to buy the computer
    if(costOfComputer > bankBalance){
        alert("Your bank balance is to low")
    }else {
        //cost of the computer is deducted from bank balance
        bankBalance = bankBalance - costOfComputer
        displayBankBalance();
        //Alert message with the computer bought
        alert("Congrats! You are the owner of "+ computers[index].title)
        computerBought = true;
    }
}

//Click event listener for loan button
function getALoanClick(){

    if(bankBalance <= 0){
        alert("you can't get a loan if you dont have money in the bank")
        return;
    }

    /*They already have a loan and can not get another one
    */
    if(loanAmount > 0){
        alert("You already have a loan, pay the bank before you can get a new one")
        return;
    }
    /*if they have a loan and not bought a computer yet - the get prompt with a message and can not get a loan
    before buying a computer once - after a computer bought they can get a new loan every time they pay up the
    outstanding loan
     */
    if (loanTaken && !computerBought){
        alert("Before you can take another loan you have to buy a computer")
        return;
    }

    //ask the user once for a loan amount before checking the condition of loan amount
    //or if the the user canceled
    do{
        loanAmount = prompt("Enter amount for loan")
        //if the user cancel the prompt box - the prompt goes away
        if(loanAmount == "null" || loanAmount == null || loanAmount == "" ) return
        loanAmount = parseInt(loanAmount)

        //check if the value is nan - it could not parse and is not a numeric number - not valid and then it prompts the user again
    }while(isNaN(loanAmount))

    //check the condition that the loan amount can not be more the twice of the bank balance
    if(loanAmount > bankBalance*2){
        alert("Your not allowed a loan more then double your bank balance")
    }else{
            bankBalance+=loanAmount
            displayOutstandingLoan();
            displayBankBalance();
            loanTaken = true;
            hidePayButton();
    }
}

//on pay loan button click
function payLoanBtnClick(){

    //checks if loan is paid - returns from function if paid
    if(loanAmount <= 0){
        alert("Your loan is fully paid")
        return;
    }

    //Checks if there is money to pay the loan - returns from function if not money to pay
    if(payAmount <= 0){
        alert("You have no money to pay the loan")
        return;
    }

    //if pay amount is greater the loan amount - to avoid negative values - same for the else and loan amount
    if(loanAmount<=payAmount){
        payAmount-=loanAmount
        loanAmount = 0
        hidePayButton()
    }else{
        loanAmount -= payAmount
        payAmount = 0
    }

    displayOutstandingLoan();
    displayPay();
}
//------------------------------------------- Helper function ---------------------------------------------//

//displays new pay value
function displayPay(){
    payAmountElement.innerText = payAmount + ' Kr';
}
//displays new loan value
function displayOutstandingLoan(){
    loanHeadingElement.innerText = "Outstanding loan:"
    outstandingLoanElement.innerText =  '-' +loanAmount + ' Kr';
}

//displays new bank balance value
function displayBankBalance(){
    balanceElement.innerText = bankBalance + ' Kr';
}

//helper function for hiding the pay button whenever there is no loan to be paid
function hidePayButton(){

    if (payAmountBtnElement.style.display === "none") {
        payAmountBtnElement.style.display = "block";
    } else {
        payAmountBtnElement.style.display = "none";
    }
}

//catching the change in computer selected
computersElement.addEventListener("change",handleComputerChange)

//catching the error when image is broken
imageElement.addEventListener("error", function(event) {
    event.target.src = "images/img_3.png"
    event.onerror = null
})



