# Bank Website 

Assignment was to build a dynamic webpage using plain JavaScript, HTML and CSS.

## Link to view project
Link to view the project: https://miakristiansen.gitlab.io/-/assignment_1_bankwebsite/-/jobs/1583288382/artifacts/public/index.html

## Wireframe
![Screenshot](wireframe.png)
